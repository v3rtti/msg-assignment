package main

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	hubutils "go-gist.appspot.com/orktes/c5e76b7dd035ee25057e"
	"testing"
)

func TestMsgAssignment(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "MsgAssignment Suite")
}

func TestJaakonPerformance(t *testing.T) {
	println("Running performance test")
	hub := NewHub()
	hubutils.RunMessageHubTests(&hub, MessageEncoder)
}
