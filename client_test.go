package main

import (
	"bytes"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Client", func() {

	Describe("With a Msg", func() {
		It("should marshal and unmarshal to original", func() {
			originalMsg := Msg{
				Receivers: []uint64{1, 2},
				Payload:   []byte("foobar"),
			}

			converted := ConvertMsgToBytes(originalMsg)
			result, err := ConvertBytesToMsg(bytes.NewReader(converted))

			Expect(err).NotTo(HaveOccurred())
			Expect(result).To(Equal(originalMsg))
		})
	})

	It("should return error with empty data conversion", func() {
		_, err := ConvertBytesToMsg(bytes.NewReader([]byte{}))
		Expect(err).To(HaveOccurred())
	})
})
