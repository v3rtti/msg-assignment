package main

import (
	"encoding/binary"
	"errors"
	"github.com/golang/protobuf/proto"
	"io"
	"io/ioutil"
	"log"
)

type Client struct {
	conn    io.ReadWriter
	channel chan []byte
	user_id uint64
}

func MessageEncoder(receivers []uint64, payload []byte) []byte {
	return ConvertMsgToBytes(Msg{
		Receivers: receivers,
		Payload:   payload,
	})
}

func ConvertMsgToBytes(msg Msg) []byte {
	data, _ := proto.Marshal(&msg)
	msgLengthBytes := make([]byte, 4)

	binary.LittleEndian.PutUint32(msgLengthBytes, uint32(len(data)))
	return append(msgLengthBytes, data...)
}

func getMsgLength(reader io.Reader) (int64, error) {
	msgLengthBytes := make([]byte, 4)
	n, err := reader.Read(msgLengthBytes)

	if n != 4 || err != nil {
		return -1, errors.New("Failed to get length")
	}
	msglength := binary.LittleEndian.Uint32(msgLengthBytes)
	return int64(msglength), nil
}

func ConvertBytesToMsg(reader io.Reader) (Msg, error) {
	msg := &Msg{}

	msgLength, lenError := getMsgLength(reader)
	if lenError != nil {
		return *msg, lenError
	}

	limitReader := io.LimitReader(reader, msgLength)
	msgBytes, _ := ioutil.ReadAll(limitReader)

	unmarshalErr := proto.Unmarshal(msgBytes, msg)
	if unmarshalErr != nil {
		log.Fatal("unmarshaling error: ", unmarshalErr)
		return *msg, unmarshalErr
	}
	return *msg, nil
}

func (c Client) ReadMsgsInto(ch chan<- Msg) {
	for {
		msg, err := ConvertBytesToMsg(c.conn)
		if err != nil {
			break
		}
		ch <- msg
	}
}

func (c Client) WriteMsgFrom(ch <-chan []byte) {
	for payload := range ch {
		_, err := c.conn.Write(payload)
		if err != nil {
			break
		}
	}
}
