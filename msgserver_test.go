package main

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"net"
	"time"
)

var _ = Describe("Msgserver", func() {

	Describe("When starting server", func() {

		const address string = "localhost:6000"
		var err error
		var conn net.Conn
		var msgchan chan Msg
		var addchan chan Client
		var rmchan chan Client
		var hub Hub

		BeforeEach(func() {
			hub = NewHub()
			msgchan = make(chan Msg)
			addchan = make(chan Client)
			rmchan = make(chan Client)
			go hub.startServer(address, msgchan, addchan, rmchan)
			time.Sleep(time.Millisecond * 200)
			conn, err = net.Dial("tcp", address)
			<-addchan
		})

		It("Should accept a connection", func() {
			Expect(err).NotTo(HaveOccurred())
		})

		It("Should pass the incoming message to channel", func() {
			testMsg := Msg{
				Receivers: []uint64{1, 2},
				Payload:   []byte("foobar"),
			}
			conn.Write(ConvertMsgToBytes(testMsg))
			response := <-msgchan
			Expect(response.Payload).To(Equal([]byte("foobar")))
		})

		It("Should succesfully parse multiple messages", func() {
			testMsg1 := Msg{
				Receivers: []uint64{1, 2},
				Payload:   []byte("foobar"),
			}
			testMsg2 := Msg{
				Receivers: []uint64{1, 2},
				Payload:   []byte("barbaz"),
			}
			conn.Write(ConvertMsgToBytes(testMsg1))
			conn.Write(ConvertMsgToBytes(testMsg2))
			response1 := <-msgchan
			Expect(response1.Payload).To(Equal([]byte("foobar")))
			response2 := <-msgchan
			Expect(response2.Payload).To(Equal([]byte("barbaz")))
		})

		AfterEach(func() {
			conn.Close()
			hub.Stop()
		})
	})

	Describe("With a hub", func() {
		It("Should list client ID's", func() {
			s := NewHub()
			s.clients[1] = make(chan<- []byte)
			s.clients[2] = make(chan<- []byte)

			Expect(s.ClientIDs()).To(ContainElement(uint64(1)))
			Expect(s.ClientIDs()).To(ContainElement(uint64(2)))
		})
	})
})
