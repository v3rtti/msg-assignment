package main

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Messagehandler", func() {

	Describe("When sending messages", func() {

		var msgchan chan Msg
		var addchan chan Client
		var rmchan chan Client
		var stopchan chan bool
		var hub Hub

		BeforeEach(func() {
			msgchan = make(chan Msg)
			addchan = make(chan Client)
			rmchan = make(chan Client)
			stopchan = make(chan bool)
			hub = NewHub()
			go hub.handleMessages(msgchan, addchan, rmchan, stopchan)
		})

		Describe("With two clients", func() {
			var client1 Client
			var client2 Client
			var channel1 chan []byte
			var channel2 chan []byte

			BeforeEach(func() {
				channel1 = make(chan []byte)
				client1 = Client{nil, channel1, 1}
				channel2 = make(chan []byte)
				client2 = Client{nil, channel2, 2}
				addchan <- client1
				addchan <- client2
			})

			It("Should pass message only to correct receiver", func() {
				for len(hub.ClientIDs()) < 2 {
					// wait until clients are registered
				}
				msgToClient2 := &Msg{
					Receivers: []uint64{2},
					Payload:   []byte("foo"),
				}
				msgchan <- *msgToClient2

				Eventually(channel2).Should(Receive(Equal([]byte("foo"))))
				Consistently(channel1).ShouldNot(Receive())
			})

			It("Should have two clients", func() {
				for len(hub.ClientIDs()) < 2 {
					// wait until clients are registered
				}
				Eventually(len(hub.ClientIDs())).Should(Equal(2))
			})

			AfterEach(func() {
				rmchan <- client1
				rmchan <- client2
				close(channel1)
				close(channel2)
				Eventually(len(hub.ClientIDs())).Should(Equal(0))
			})
		})

		AfterEach(func() {
			stopchan <- true
			close(addchan)
			close(msgchan)
			close(stopchan)
		})
	})
})
