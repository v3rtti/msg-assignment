package main

import (
	"io"
	"log"
	"net"
	"sync"
)

type Hub struct {
	clients map[uint64]chan<- []byte
	ln      net.Listener
	mutex   sync.Mutex
}

var latest_user_id uint64 = 1

func main() {
	hub := NewHub()
	hub.Start("localhost:6000")
}

func NewHub() Hub {
	newHub := Hub{}
	//	newHub.mutex = &sync.Mutex{}
	newHub.clients = make(map[uint64]chan<- []byte)

	return newHub
}

func (s *Hub) Start(address string) error {
	addchan := make(chan Client)
	rmchan := make(chan Client)
	msgchan := make(chan Msg)
	stopchan := make(chan bool)

	go s.handleMessages(msgchan, addchan, rmchan, stopchan)
	s.startServer(address, msgchan, addchan, rmchan)
	return nil
}

func (s *Hub) startServer(address string, msgchan chan<- Msg, addchan chan<- Client, rmchan chan<- Client) error {
	latest_user_id = 0
	var err error
	s.ln, err = net.Listen("tcp", address)
	if err != nil {
		log.Println("Failed to start")
		return err
	}

	for {
		conn, err := s.ln.Accept()
		if err != nil {
			return err
		}
		latest_user_id = latest_user_id + 1
		go handleConnection(latest_user_id, conn, msgchan, addchan, rmchan)
	}
	return nil
}

func handleConnection(user_id uint64, c io.ReadWriteCloser, messageChannel chan<- Msg, clientChannel chan<- Client, removeChannel chan<- Client) {
	ch := make(chan []byte)
	client := Client{c, ch, user_id}
	clientChannel <- client

	defer func() {
		c.Close()
		removeChannel <- client
	}()

	go client.ReadMsgsInto(messageChannel)
	client.WriteMsgFrom(client.channel)
}

func (s *Hub) Stop() error {
	return s.ln.Close()
}

func (s *Hub) ClientIDs() []uint64 {
	s.mutex.Lock()

	ids := make([]uint64, 0, len(s.clients))
	for k := range s.clients {
		ids = append(ids, k)
	}
	s.mutex.Unlock()

	return ids
}
