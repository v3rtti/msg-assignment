package main

func (hub *Hub) handleMessages(msgchan <-chan Msg, addchan <-chan Client, rmchan <-chan Client, stopchan <-chan bool) {
outer:
	for {
		select {
		case msg := <-msgchan:
			hub.mutex.Lock()
			for _, user_id := range msg.Receivers {
				payload := msg.Payload
				target := hub.clients[user_id]
				if target != nil {
					go func(mch chan<- []byte) {
						mch <- payload
					}(target)
				}
			}
			hub.mutex.Unlock()
		case client := <-addchan:
			hub.mutex.Lock()
			hub.clients[client.user_id] = client.channel
			hub.mutex.Unlock()
		case client := <-rmchan:
			hub.mutex.Lock()
			delete(hub.clients, client.user_id)
			hub.mutex.Unlock()
		case <-stopchan:
			break outer
		}
	}
}
